﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GraphicalRulesManageConsole.Components;

namespace GraphicalRulesManageConsole
{
    public partial class NewRuleForm : Form
    {
        JsonObjectManager jsonObjectManager = new JsonObjectManager();
        public NewRuleForm()
        {
            InitializeComponent();
        }

        private void createNewBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ruleNameTextBox.Text))
            {
                MessageBox.Show(@"You must specify rule name.", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
            var graphicalRulesPath = clientSection.Settings.Get("graphicalRulesPath").Value.ValueXml.InnerText;
            var newBpmnFilePath = graphicalRulesPath + @"\" + ruleNameTextBox.Text + ".bpmn";
            const string newFileContent = @"<?xml version=""1.0"" encoding=""UTF-8""?>
                                    <bpmn:definitions xmlns:bpmn=""http://www.omg.org/spec/BPMN/20100524/MODEL"" xmlns:bpmndi=""http://www.omg.org/spec/BPMN/20100524/DI"" xmlns:di=""http://www.omg.org/spec/DD/20100524/DI"" xmlns:dc=""http://www.omg.org/spec/DD/20100524/DC"" id=""Definitions_1"" targetNamespace=""http://bpmn.io/schema/bpmn"">
                                      <bpmn:process id=""Process_1"" isExecutable=""false"">
                                        <bpmn:startEvent id=""StartEvent_1"" />
                                      </bpmn:process>
                                      <bpmndi:BPMNDiagram id=""BPMNDiagram_1"">
                                        <bpmndi:BPMNPlane id=""BPMNPlane_1"" bpmnElement=""Process_1"">
                                          <bpmndi:BPMNShape id=""_BPMNShape_StartEvent_2"" bpmnElement=""StartEvent_1"">
                                            <dc:Bounds x=""173"" y=""102"" width=""36"" height=""36"" />
                                          </bpmndi:BPMNShape>
                                        </bpmndi:BPMNPlane>
                                      </bpmndi:BPMNDiagram>
                                    </bpmn:definitions>";

            using (StreamWriter sw = new StreamWriter(newBpmnFilePath, true))
            {
                sw.WriteLine(newFileContent);
                sw.Close();
            }

            var cmdCommandsManager = new CmdCommandsManager();
            cmdCommandsManager.OpenCamundaModeler(newBpmnFilePath);
            Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
