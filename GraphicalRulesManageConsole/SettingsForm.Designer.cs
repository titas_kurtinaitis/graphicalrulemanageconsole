﻿namespace GraphicalRulesManageConsole
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.changeCamundaModelerTemplatesPathButton = new System.Windows.Forms.Button();
            this.camundaModelerTemplatesPathTextBox = new System.Windows.Forms.TextBox();
            this.camundaModelerTemplatesPathLabel = new System.Windows.Forms.Label();
            this.openhabScriptsPathLabel = new System.Windows.Forms.Label();
            this.openhabItemsPathLabel = new System.Windows.Forms.Label();
            this.openhabScriptsPathTextBox = new System.Windows.Forms.TextBox();
            this.openhabItemsPathTextBox = new System.Windows.Forms.TextBox();
            this.changeOpenhabScriptsPathButton = new System.Windows.Forms.Button();
            this.changeOpenhabItemsPathButton = new System.Windows.Forms.Button();
            this.changeBpmnToRulesConverterFilePathButton = new System.Windows.Forms.Button();
            this.bpmnToRulesConverterFilePathTextBox = new System.Windows.Forms.TextBox();
            this.bpmnToRulesConverterFilePathLabel = new System.Windows.Forms.Label();
            this.openhabRulesPathLabel = new System.Windows.Forms.Label();
            this.changeOpenhabRulesPathButton = new System.Windows.Forms.Button();
            this.openhabRulesPathTextBox = new System.Windows.Forms.TextBox();
            this.changeCamundaModelerExecutableFilePathButton = new System.Windows.Forms.Button();
            this.camundaModelerExecutableFilePathLabel = new System.Windows.Forms.Label();
            this.camundaModelerExecutableFilePathTextBox = new System.Windows.Forms.TextBox();
            this.graphicalRulesPathLabel = new System.Windows.Forms.Label();
            this.graphicalRulesTextBox = new System.Windows.Forms.TextBox();
            this.graphicalRulesPathChangeBtn = new System.Windows.Forms.Button();
            this.openHabUrlLabel = new System.Windows.Forms.Label();
            this.openHabUrlTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // changeCamundaModelerTemplatesPathButton
            // 
            this.changeCamundaModelerTemplatesPathButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.changeCamundaModelerTemplatesPathButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeCamundaModelerTemplatesPathButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.changeCamundaModelerTemplatesPathButton.FlatAppearance.BorderSize = 0;
            this.changeCamundaModelerTemplatesPathButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.DimGray;
            this.changeCamundaModelerTemplatesPathButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.changeCamundaModelerTemplatesPathButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.changeCamundaModelerTemplatesPathButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeCamundaModelerTemplatesPathButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeCamundaModelerTemplatesPathButton.Location = new System.Drawing.Point(520, 50);
            this.changeCamundaModelerTemplatesPathButton.Name = "changeCamundaModelerTemplatesPathButton";
            this.changeCamundaModelerTemplatesPathButton.Size = new System.Drawing.Size(100, 30);
            this.changeCamundaModelerTemplatesPathButton.TabIndex = 0;
            this.changeCamundaModelerTemplatesPathButton.TabStop = false;
            this.changeCamundaModelerTemplatesPathButton.Text = "Change";
            this.changeCamundaModelerTemplatesPathButton.UseVisualStyleBackColor = false;
            this.changeCamundaModelerTemplatesPathButton.Click += new System.EventHandler(this.changeCamundaModelerTemplatesPathButton_Click);
            // 
            // camundaModelerTemplatesPathTextBox
            // 
            this.camundaModelerTemplatesPathTextBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.camundaModelerTemplatesPathTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.camundaModelerTemplatesPathTextBox.ForeColor = System.Drawing.Color.Black;
            this.camundaModelerTemplatesPathTextBox.Location = new System.Drawing.Point(30, 50);
            this.camundaModelerTemplatesPathTextBox.Multiline = true;
            this.camundaModelerTemplatesPathTextBox.Name = "camundaModelerTemplatesPathTextBox";
            this.camundaModelerTemplatesPathTextBox.ReadOnly = true;
            this.camundaModelerTemplatesPathTextBox.Size = new System.Drawing.Size(480, 30);
            this.camundaModelerTemplatesPathTextBox.TabIndex = 1;
            this.camundaModelerTemplatesPathTextBox.TabStop = false;
            // 
            // camundaModelerTemplatesPathLabel
            // 
            this.camundaModelerTemplatesPathLabel.AutoSize = true;
            this.camundaModelerTemplatesPathLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.camundaModelerTemplatesPathLabel.ForeColor = System.Drawing.Color.White;
            this.camundaModelerTemplatesPathLabel.Location = new System.Drawing.Point(25, 20);
            this.camundaModelerTemplatesPathLabel.Name = "camundaModelerTemplatesPathLabel";
            this.camundaModelerTemplatesPathLabel.Size = new System.Drawing.Size(324, 22);
            this.camundaModelerTemplatesPathLabel.TabIndex = 2;
            this.camundaModelerTemplatesPathLabel.Text = "Camunda Modeler Templates Path";
            // 
            // openhabScriptsPathLabel
            // 
            this.openhabScriptsPathLabel.AutoSize = true;
            this.openhabScriptsPathLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openhabScriptsPathLabel.ForeColor = System.Drawing.Color.White;
            this.openhabScriptsPathLabel.Location = new System.Drawing.Point(25, 90);
            this.openhabScriptsPathLabel.Name = "openhabScriptsPathLabel";
            this.openhabScriptsPathLabel.Size = new System.Drawing.Size(215, 22);
            this.openhabScriptsPathLabel.TabIndex = 3;
            this.openhabScriptsPathLabel.Text = "openHAB Scripts Path";
            // 
            // openhabItemsPathLabel
            // 
            this.openhabItemsPathLabel.AutoSize = true;
            this.openhabItemsPathLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openhabItemsPathLabel.ForeColor = System.Drawing.Color.White;
            this.openhabItemsPathLabel.Location = new System.Drawing.Point(25, 160);
            this.openhabItemsPathLabel.Name = "openhabItemsPathLabel";
            this.openhabItemsPathLabel.Size = new System.Drawing.Size(200, 22);
            this.openhabItemsPathLabel.TabIndex = 4;
            this.openhabItemsPathLabel.Text = "openHAB Items Path";
            // 
            // openhabScriptsPathTextBox
            // 
            this.openhabScriptsPathTextBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.openhabScriptsPathTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openhabScriptsPathTextBox.ForeColor = System.Drawing.Color.Black;
            this.openhabScriptsPathTextBox.Location = new System.Drawing.Point(30, 120);
            this.openhabScriptsPathTextBox.Multiline = true;
            this.openhabScriptsPathTextBox.Name = "openhabScriptsPathTextBox";
            this.openhabScriptsPathTextBox.ReadOnly = true;
            this.openhabScriptsPathTextBox.Size = new System.Drawing.Size(480, 30);
            this.openhabScriptsPathTextBox.TabIndex = 5;
            this.openhabScriptsPathTextBox.TabStop = false;
            // 
            // openhabItemsPathTextBox
            // 
            this.openhabItemsPathTextBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.openhabItemsPathTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openhabItemsPathTextBox.ForeColor = System.Drawing.Color.Black;
            this.openhabItemsPathTextBox.Location = new System.Drawing.Point(30, 190);
            this.openhabItemsPathTextBox.Multiline = true;
            this.openhabItemsPathTextBox.Name = "openhabItemsPathTextBox";
            this.openhabItemsPathTextBox.ReadOnly = true;
            this.openhabItemsPathTextBox.Size = new System.Drawing.Size(480, 30);
            this.openhabItemsPathTextBox.TabIndex = 6;
            this.openhabItemsPathTextBox.TabStop = false;
            // 
            // changeOpenhabScriptsPathButton
            // 
            this.changeOpenhabScriptsPathButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.changeOpenhabScriptsPathButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeOpenhabScriptsPathButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.changeOpenhabScriptsPathButton.FlatAppearance.BorderSize = 0;
            this.changeOpenhabScriptsPathButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabScriptsPathButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabScriptsPathButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabScriptsPathButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeOpenhabScriptsPathButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeOpenhabScriptsPathButton.Location = new System.Drawing.Point(520, 120);
            this.changeOpenhabScriptsPathButton.Name = "changeOpenhabScriptsPathButton";
            this.changeOpenhabScriptsPathButton.Size = new System.Drawing.Size(100, 30);
            this.changeOpenhabScriptsPathButton.TabIndex = 7;
            this.changeOpenhabScriptsPathButton.TabStop = false;
            this.changeOpenhabScriptsPathButton.Text = "Change";
            this.changeOpenhabScriptsPathButton.UseVisualStyleBackColor = false;
            this.changeOpenhabScriptsPathButton.Click += new System.EventHandler(this.changeOpenhabScriptsPathButton_Click);
            // 
            // changeOpenhabItemsPathButton
            // 
            this.changeOpenhabItemsPathButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.changeOpenhabItemsPathButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeOpenhabItemsPathButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.changeOpenhabItemsPathButton.FlatAppearance.BorderSize = 0;
            this.changeOpenhabItemsPathButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabItemsPathButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabItemsPathButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabItemsPathButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeOpenhabItemsPathButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeOpenhabItemsPathButton.Location = new System.Drawing.Point(520, 190);
            this.changeOpenhabItemsPathButton.Name = "changeOpenhabItemsPathButton";
            this.changeOpenhabItemsPathButton.Size = new System.Drawing.Size(100, 30);
            this.changeOpenhabItemsPathButton.TabIndex = 8;
            this.changeOpenhabItemsPathButton.TabStop = false;
            this.changeOpenhabItemsPathButton.Text = "Change";
            this.changeOpenhabItemsPathButton.UseVisualStyleBackColor = false;
            this.changeOpenhabItemsPathButton.Click += new System.EventHandler(this.changeOpenhabItemsPathButton_Click);
            // 
            // changeBpmnToRulesConverterFilePathButton
            // 
            this.changeBpmnToRulesConverterFilePathButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.changeBpmnToRulesConverterFilePathButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeBpmnToRulesConverterFilePathButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.changeBpmnToRulesConverterFilePathButton.FlatAppearance.BorderSize = 0;
            this.changeBpmnToRulesConverterFilePathButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.DimGray;
            this.changeBpmnToRulesConverterFilePathButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.changeBpmnToRulesConverterFilePathButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.changeBpmnToRulesConverterFilePathButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeBpmnToRulesConverterFilePathButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeBpmnToRulesConverterFilePathButton.Location = new System.Drawing.Point(520, 330);
            this.changeBpmnToRulesConverterFilePathButton.Name = "changeBpmnToRulesConverterFilePathButton";
            this.changeBpmnToRulesConverterFilePathButton.Size = new System.Drawing.Size(100, 30);
            this.changeBpmnToRulesConverterFilePathButton.TabIndex = 9;
            this.changeBpmnToRulesConverterFilePathButton.TabStop = false;
            this.changeBpmnToRulesConverterFilePathButton.Text = "Change";
            this.changeBpmnToRulesConverterFilePathButton.UseVisualStyleBackColor = false;
            this.changeBpmnToRulesConverterFilePathButton.Click += new System.EventHandler(this.changeBpmnToRulesConverterFilePathButton_Click);
            // 
            // bpmnToRulesConverterFilePathTextBox
            // 
            this.bpmnToRulesConverterFilePathTextBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.bpmnToRulesConverterFilePathTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bpmnToRulesConverterFilePathTextBox.ForeColor = System.Drawing.Color.Black;
            this.bpmnToRulesConverterFilePathTextBox.Location = new System.Drawing.Point(30, 330);
            this.bpmnToRulesConverterFilePathTextBox.Multiline = true;
            this.bpmnToRulesConverterFilePathTextBox.Name = "bpmnToRulesConverterFilePathTextBox";
            this.bpmnToRulesConverterFilePathTextBox.ReadOnly = true;
            this.bpmnToRulesConverterFilePathTextBox.Size = new System.Drawing.Size(480, 30);
            this.bpmnToRulesConverterFilePathTextBox.TabIndex = 10;
            this.bpmnToRulesConverterFilePathTextBox.TabStop = false;
            // 
            // bpmnToRulesConverterFilePathLabel
            // 
            this.bpmnToRulesConverterFilePathLabel.AutoSize = true;
            this.bpmnToRulesConverterFilePathLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bpmnToRulesConverterFilePathLabel.ForeColor = System.Drawing.Color.White;
            this.bpmnToRulesConverterFilePathLabel.Location = new System.Drawing.Point(25, 300);
            this.bpmnToRulesConverterFilePathLabel.Name = "bpmnToRulesConverterFilePathLabel";
            this.bpmnToRulesConverterFilePathLabel.Size = new System.Drawing.Size(349, 22);
            this.bpmnToRulesConverterFilePathLabel.TabIndex = 11;
            this.bpmnToRulesConverterFilePathLabel.Text = "BPMN file to Rules converter file path";
            // 
            // openhabRulesPathLabel
            // 
            this.openhabRulesPathLabel.AutoSize = true;
            this.openhabRulesPathLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openhabRulesPathLabel.ForeColor = System.Drawing.Color.White;
            this.openhabRulesPathLabel.Location = new System.Drawing.Point(25, 230);
            this.openhabRulesPathLabel.Name = "openhabRulesPathLabel";
            this.openhabRulesPathLabel.Size = new System.Drawing.Size(200, 22);
            this.openhabRulesPathLabel.TabIndex = 12;
            this.openhabRulesPathLabel.Text = "openHAB Rules Path";
            // 
            // changeOpenhabRulesPathButton
            // 
            this.changeOpenhabRulesPathButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.changeOpenhabRulesPathButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeOpenhabRulesPathButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.changeOpenhabRulesPathButton.FlatAppearance.BorderSize = 0;
            this.changeOpenhabRulesPathButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabRulesPathButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabRulesPathButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.changeOpenhabRulesPathButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeOpenhabRulesPathButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeOpenhabRulesPathButton.Location = new System.Drawing.Point(520, 260);
            this.changeOpenhabRulesPathButton.Name = "changeOpenhabRulesPathButton";
            this.changeOpenhabRulesPathButton.Size = new System.Drawing.Size(100, 30);
            this.changeOpenhabRulesPathButton.TabIndex = 13;
            this.changeOpenhabRulesPathButton.TabStop = false;
            this.changeOpenhabRulesPathButton.Text = "Change";
            this.changeOpenhabRulesPathButton.UseVisualStyleBackColor = false;
            this.changeOpenhabRulesPathButton.Click += new System.EventHandler(this.changeOpenhabRulesPathButton_Click);
            // 
            // openhabRulesPathTextBox
            // 
            this.openhabRulesPathTextBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.openhabRulesPathTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openhabRulesPathTextBox.ForeColor = System.Drawing.Color.Black;
            this.openhabRulesPathTextBox.Location = new System.Drawing.Point(30, 260);
            this.openhabRulesPathTextBox.Multiline = true;
            this.openhabRulesPathTextBox.Name = "openhabRulesPathTextBox";
            this.openhabRulesPathTextBox.ReadOnly = true;
            this.openhabRulesPathTextBox.Size = new System.Drawing.Size(480, 30);
            this.openhabRulesPathTextBox.TabIndex = 14;
            this.openhabRulesPathTextBox.TabStop = false;
            // 
            // changeCamundaModelerExecutableFilePathButton
            // 
            this.changeCamundaModelerExecutableFilePathButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.changeCamundaModelerExecutableFilePathButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.changeCamundaModelerExecutableFilePathButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.changeCamundaModelerExecutableFilePathButton.FlatAppearance.BorderSize = 0;
            this.changeCamundaModelerExecutableFilePathButton.FlatAppearance.CheckedBackColor = System.Drawing.Color.DimGray;
            this.changeCamundaModelerExecutableFilePathButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.changeCamundaModelerExecutableFilePathButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.changeCamundaModelerExecutableFilePathButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changeCamundaModelerExecutableFilePathButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeCamundaModelerExecutableFilePathButton.Location = new System.Drawing.Point(520, 400);
            this.changeCamundaModelerExecutableFilePathButton.Name = "changeCamundaModelerExecutableFilePathButton";
            this.changeCamundaModelerExecutableFilePathButton.Size = new System.Drawing.Size(100, 30);
            this.changeCamundaModelerExecutableFilePathButton.TabIndex = 15;
            this.changeCamundaModelerExecutableFilePathButton.TabStop = false;
            this.changeCamundaModelerExecutableFilePathButton.Text = "Change";
            this.changeCamundaModelerExecutableFilePathButton.UseVisualStyleBackColor = false;
            this.changeCamundaModelerExecutableFilePathButton.Click += new System.EventHandler(this.changeCamundaModelerExecutableFilePathButton_Click);
            // 
            // camundaModelerExecutableFilePathLabel
            // 
            this.camundaModelerExecutableFilePathLabel.AutoSize = true;
            this.camundaModelerExecutableFilePathLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.camundaModelerExecutableFilePathLabel.Location = new System.Drawing.Point(25, 370);
            this.camundaModelerExecutableFilePathLabel.Name = "camundaModelerExecutableFilePathLabel";
            this.camundaModelerExecutableFilePathLabel.Size = new System.Drawing.Size(362, 22);
            this.camundaModelerExecutableFilePathLabel.TabIndex = 16;
            this.camundaModelerExecutableFilePathLabel.Text = "Camunda Modeler Executable file Path";
            // 
            // camundaModelerExecutableFilePathTextBox
            // 
            this.camundaModelerExecutableFilePathTextBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.camundaModelerExecutableFilePathTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.camundaModelerExecutableFilePathTextBox.ForeColor = System.Drawing.Color.Black;
            this.camundaModelerExecutableFilePathTextBox.Location = new System.Drawing.Point(30, 400);
            this.camundaModelerExecutableFilePathTextBox.Multiline = true;
            this.camundaModelerExecutableFilePathTextBox.Name = "camundaModelerExecutableFilePathTextBox";
            this.camundaModelerExecutableFilePathTextBox.ReadOnly = true;
            this.camundaModelerExecutableFilePathTextBox.Size = new System.Drawing.Size(480, 30);
            this.camundaModelerExecutableFilePathTextBox.TabIndex = 17;
            this.camundaModelerExecutableFilePathTextBox.TabStop = false;
            // 
            // graphicalRulesPathLabel
            // 
            this.graphicalRulesPathLabel.AutoSize = true;
            this.graphicalRulesPathLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.graphicalRulesPathLabel.Location = new System.Drawing.Point(25, 440);
            this.graphicalRulesPathLabel.Name = "graphicalRulesPathLabel";
            this.graphicalRulesPathLabel.Size = new System.Drawing.Size(202, 22);
            this.graphicalRulesPathLabel.TabIndex = 18;
            this.graphicalRulesPathLabel.Text = "Graphical Rules Path";
            // 
            // graphicalRulesTextBox
            // 
            this.graphicalRulesTextBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.graphicalRulesTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.graphicalRulesTextBox.ForeColor = System.Drawing.Color.Black;
            this.graphicalRulesTextBox.Location = new System.Drawing.Point(30, 470);
            this.graphicalRulesTextBox.Multiline = true;
            this.graphicalRulesTextBox.Name = "graphicalRulesTextBox";
            this.graphicalRulesTextBox.ReadOnly = true;
            this.graphicalRulesTextBox.Size = new System.Drawing.Size(480, 30);
            this.graphicalRulesTextBox.TabIndex = 19;
            this.graphicalRulesTextBox.TabStop = false;
            // 
            // graphicalRulesPathChangeBtn
            // 
            this.graphicalRulesPathChangeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.graphicalRulesPathChangeBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.graphicalRulesPathChangeBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.graphicalRulesPathChangeBtn.FlatAppearance.BorderSize = 0;
            this.graphicalRulesPathChangeBtn.FlatAppearance.CheckedBackColor = System.Drawing.Color.DimGray;
            this.graphicalRulesPathChangeBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.graphicalRulesPathChangeBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.graphicalRulesPathChangeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.graphicalRulesPathChangeBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.graphicalRulesPathChangeBtn.Location = new System.Drawing.Point(520, 470);
            this.graphicalRulesPathChangeBtn.Name = "graphicalRulesPathChangeBtn";
            this.graphicalRulesPathChangeBtn.Size = new System.Drawing.Size(100, 30);
            this.graphicalRulesPathChangeBtn.TabIndex = 20;
            this.graphicalRulesPathChangeBtn.TabStop = false;
            this.graphicalRulesPathChangeBtn.Text = "Change";
            this.graphicalRulesPathChangeBtn.UseVisualStyleBackColor = false;
            this.graphicalRulesPathChangeBtn.Click += new System.EventHandler(this.graphicalRulesPathChangeBtn_Click);
            // 
            // openHabUrlLabel
            // 
            this.openHabUrlLabel.AutoSize = true;
            this.openHabUrlLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openHabUrlLabel.Location = new System.Drawing.Point(25, 510);
            this.openHabUrlLabel.Name = "openHabUrlLabel";
            this.openHabUrlLabel.Size = new System.Drawing.Size(141, 22);
            this.openHabUrlLabel.TabIndex = 21;
            this.openHabUrlLabel.Text = "openHAB URL";
            // 
            // openHabUrlTextBox
            // 
            this.openHabUrlTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openHabUrlTextBox.Location = new System.Drawing.Point(30, 540);
            this.openHabUrlTextBox.Name = "openHabUrlTextBox";
            this.openHabUrlTextBox.Size = new System.Drawing.Size(480, 26);
            this.openHabUrlTextBox.TabIndex = 22;
            this.openHabUrlTextBox.TextChanged += new System.EventHandler(this.openHabUrlTextBox_TextChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(141)))), ((int)(((byte)(188)))));
            this.ClientSize = new System.Drawing.Size(645, 618);
            this.Controls.Add(this.openHabUrlTextBox);
            this.Controls.Add(this.openHabUrlLabel);
            this.Controls.Add(this.graphicalRulesPathChangeBtn);
            this.Controls.Add(this.graphicalRulesTextBox);
            this.Controls.Add(this.graphicalRulesPathLabel);
            this.Controls.Add(this.camundaModelerExecutableFilePathTextBox);
            this.Controls.Add(this.camundaModelerExecutableFilePathLabel);
            this.Controls.Add(this.changeCamundaModelerExecutableFilePathButton);
            this.Controls.Add(this.openhabRulesPathTextBox);
            this.Controls.Add(this.changeOpenhabRulesPathButton);
            this.Controls.Add(this.openhabRulesPathLabel);
            this.Controls.Add(this.bpmnToRulesConverterFilePathLabel);
            this.Controls.Add(this.bpmnToRulesConverterFilePathTextBox);
            this.Controls.Add(this.changeBpmnToRulesConverterFilePathButton);
            this.Controls.Add(this.changeOpenhabItemsPathButton);
            this.Controls.Add(this.changeOpenhabScriptsPathButton);
            this.Controls.Add(this.openhabItemsPathTextBox);
            this.Controls.Add(this.openhabScriptsPathTextBox);
            this.Controls.Add(this.openhabItemsPathLabel);
            this.Controls.Add(this.openhabScriptsPathLabel);
            this.Controls.Add(this.camundaModelerTemplatesPathLabel);
            this.Controls.Add(this.camundaModelerTemplatesPathTextBox);
            this.Controls.Add(this.changeCamundaModelerTemplatesPathButton);
            this.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button changeCamundaModelerTemplatesPathButton;
        private System.Windows.Forms.TextBox camundaModelerTemplatesPathTextBox;
        private System.Windows.Forms.Label camundaModelerTemplatesPathLabel;
        private System.Windows.Forms.Label openhabScriptsPathLabel;
        private System.Windows.Forms.Label openhabItemsPathLabel;
        private System.Windows.Forms.TextBox openhabScriptsPathTextBox;
        private System.Windows.Forms.TextBox openhabItemsPathTextBox;
        private System.Windows.Forms.Button changeOpenhabScriptsPathButton;
        private System.Windows.Forms.Button changeOpenhabItemsPathButton;
        private System.Windows.Forms.Button changeBpmnToRulesConverterFilePathButton;
        private System.Windows.Forms.TextBox bpmnToRulesConverterFilePathTextBox;
        private System.Windows.Forms.Label bpmnToRulesConverterFilePathLabel;
        private System.Windows.Forms.Label openhabRulesPathLabel;
        private System.Windows.Forms.Button changeOpenhabRulesPathButton;
        private System.Windows.Forms.TextBox openhabRulesPathTextBox;
        private System.Windows.Forms.Button changeCamundaModelerExecutableFilePathButton;
        private System.Windows.Forms.Label camundaModelerExecutableFilePathLabel;
        private System.Windows.Forms.TextBox camundaModelerExecutableFilePathTextBox;
        private System.Windows.Forms.Label graphicalRulesPathLabel;
        private System.Windows.Forms.TextBox graphicalRulesTextBox;
        private System.Windows.Forms.Button graphicalRulesPathChangeBtn;
        private System.Windows.Forms.Label openHabUrlLabel;
        private System.Windows.Forms.TextBox openHabUrlTextBox;
    }
}