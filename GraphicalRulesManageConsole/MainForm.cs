﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GraphicalRulesManageConsole.Components;
using Newtonsoft.Json;

namespace GraphicalRulesManageConsole
{
    public partial class MainForm : Form
    {
        private FileManager fileManager = new FileManager();
        private JsonObjectManager jsonObjectManager = new JsonObjectManager();

        public MainForm()
        {
            InitializeComponent();
        }

        private void showGraphicalRulesBtn_Click(object sender, EventArgs e)
        {
            Hide();
            var graphicalRulesWindow = new GraphicalRulesWindow(ShowMainForm);
            graphicalRulesWindow.StartPosition = FormStartPosition.CenterScreen;
            graphicalRulesWindow.Show();
        }

        private void transformedRulesBtn_Click(object sender, EventArgs e)
        {
            var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
            var openHabUrl = clientSection.Settings.Get("openhabUrl").Value.ValueXml.InnerText;
            var openHabRulesWindow = openHabUrl + @"/habmin/index.html#/rules";
            try
            {
                System.Diagnostics.Process.Start(openHabRulesWindow);
            }
            catch
            {
                MessageBox.Show(@"Given url does not exist. Try different one.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateItemsAndGroupsBtn_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show(@"Do you want to update Camunda Modeler templates with item and group values?", "", MessageBoxButtons.YesNo))
            {
                var itemsManager = new ItemsManager();
                var listOfItems = itemsManager.GetListOfItems(false);
                var listOfGroups = itemsManager.GetListOfItems(true);
                if (listOfItems.Count > 0 || listOfGroups.Count > 0)
                {
                    var serializedListOfItems = JsonConvert.SerializeObject(listOfItems, Formatting.Indented);
                    var deserilizedListOfItems = JsonConvert.DeserializeObject<dynamic>(serializedListOfItems);
                    var serializedListOfGroups = JsonConvert.SerializeObject(listOfGroups, Formatting.Indented);
                    var deserilizedListOfGroups = JsonConvert.DeserializeObject<dynamic>(serializedListOfGroups);
                    var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
                    var templatesPath = clientSection.Settings.Get("camundaTemplatesPath").Value.ValueXml.InnerText;
                    var files = fileManager.GetFilesByExtension(".json", templatesPath);
                    if (files.Any())
                    {
                        foreach (var file in files)
                        {
                            var content = fileManager.GetFileContent(file);
                            var updatedContentWithItems = jsonObjectManager.UpdateItemsLists(content, deserilizedListOfItems);
                            var updatedContentWithItemsAndGroups = jsonObjectManager.UpdateGroupsLists(updatedContentWithItems, deserilizedListOfGroups);
                            fileManager.UpdateFileContent(file, updatedContentWithItemsAndGroups);
                        }
                        MessageBox.Show(@"Item and group values was updated successfully for Camunda Modeler templates.", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        MessageBox.Show(@"Template which stores group and items values was not found.", @"Error",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    
                }
                else
                {
                    MessageBox.Show(@"There wasn't any items or groups in the given items path. Nothing was updated.",
                        @"Information",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void updateScriptsBtn_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show(@"Do you want to update Camunda Modeler templates with script values?", "", MessageBoxButtons.YesNo))
            {
                var scriptsManager = new ScriptsManager();
                var listOfScripts = scriptsManager.GetListOfScripts();
                if (listOfScripts.Count > 0)
                {
                    var serializedListOfScripts = JsonConvert.SerializeObject(listOfScripts, Formatting.Indented);
                    var deserilizedListOfScripts = JsonConvert.DeserializeObject<dynamic>(serializedListOfScripts);
                    var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
                    var templatesPath = clientSection.Settings.Get("camundaTemplatesPath").Value.ValueXml.InnerText;
                    var file = fileManager.GetFileByName("operations", templatesPath);
                    if (file != null)
                    {
                        var content = fileManager.GetFileContent(file);
                        var serializedContent = jsonObjectManager.UpdateScriptsList(content, deserilizedListOfScripts);
                        fileManager.UpdateFileContent(file, serializedContent);
                        MessageBox.Show(@"Script values was updated successfully for Camunda Modeler templates.", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    else
                    {
                        MessageBox.Show(@"Template which stores script values was not found.", @"Error",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                else
                {
                    MessageBox.Show(@"There wasn't any scripts in the given scripts path. Nothing was updated.",
                        @"Information",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void settingsBtn_Click(object sender, EventArgs e)
        {
            var settingsForm = new SettingsForm();
            settingsForm.StartPosition = FormStartPosition.CenterScreen;
            settingsForm.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ShowMainForm()
        {
            Show();
        }
    }
}
