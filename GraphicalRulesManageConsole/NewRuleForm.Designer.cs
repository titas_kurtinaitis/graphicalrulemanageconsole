﻿namespace GraphicalRulesManageConsole
{
    partial class NewRuleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewRuleForm));
            this.createNewBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.ruleNameTextBox = new System.Windows.Forms.TextBox();
            this.ruleNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // createNewBtn
            // 
            this.createNewBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.createNewBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.createNewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.createNewBtn.FlatAppearance.BorderSize = 0;
            this.createNewBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.createNewBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.createNewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createNewBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.createNewBtn.ForeColor = System.Drawing.Color.White;
            this.createNewBtn.Location = new System.Drawing.Point(87, 75);
            this.createNewBtn.Name = "createNewBtn";
            this.createNewBtn.Size = new System.Drawing.Size(75, 23);
            this.createNewBtn.TabIndex = 0;
            this.createNewBtn.TabStop = false;
            this.createNewBtn.Text = "Create";
            this.createNewBtn.UseVisualStyleBackColor = false;
            this.createNewBtn.Click += new System.EventHandler(this.createNewBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.BackColor = System.Drawing.Color.White;
            this.cancelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelBtn.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.cancelBtn.FlatAppearance.BorderSize = 0;
            this.cancelBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro;
            this.cancelBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gainsboro;
            this.cancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBtn.Location = new System.Drawing.Point(181, 75);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 1;
            this.cancelBtn.TabStop = false;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = false;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // ruleNameTextBox
            // 
            this.ruleNameTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ruleNameTextBox.Location = new System.Drawing.Point(87, 31);
            this.ruleNameTextBox.Name = "ruleNameTextBox";
            this.ruleNameTextBox.Size = new System.Drawing.Size(229, 20);
            this.ruleNameTextBox.TabIndex = 2;
            // 
            // ruleNameLabel
            // 
            this.ruleNameLabel.AutoSize = true;
            this.ruleNameLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ruleNameLabel.ForeColor = System.Drawing.Color.White;
            this.ruleNameLabel.Location = new System.Drawing.Point(21, 34);
            this.ruleNameLabel.Name = "ruleNameLabel";
            this.ruleNameLabel.Size = new System.Drawing.Size(65, 12);
            this.ruleNameLabel.TabIndex = 3;
            this.ruleNameLabel.Text = "Rule Name";
            // 
            // NewRuleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(141)))), ((int)(((byte)(188)))));
            this.ClientSize = new System.Drawing.Size(339, 128);
            this.ControlBox = false;
            this.Controls.Add(this.ruleNameLabel);
            this.Controls.Add(this.ruleNameTextBox);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.createNewBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewRuleForm";
            this.Text = "New Graphical Rule";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button createNewBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.TextBox ruleNameTextBox;
        private System.Windows.Forms.Label ruleNameLabel;
    }
}