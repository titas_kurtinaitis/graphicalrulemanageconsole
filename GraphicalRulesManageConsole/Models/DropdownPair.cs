﻿namespace GraphicalRulesManageConsole.Models
{
    public class DropdownPair
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}
