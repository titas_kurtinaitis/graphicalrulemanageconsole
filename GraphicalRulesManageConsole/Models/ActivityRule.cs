﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalRulesManageConsole.Models
{
    public class ActivityRule
    {
        public string RuleName { get; set; }
        public string RulePath { get; set; }
    }
}
