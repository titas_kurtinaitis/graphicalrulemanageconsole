﻿namespace GraphicalRulesManageConsole.Enums
{
    public enum ItemTypes
    {
        Color,
        Contact,
        DateTime,
        Dimmer,
        Group,
        Number,
        Player,
        Rollershutter,
        String,
        Switch
    }
}
