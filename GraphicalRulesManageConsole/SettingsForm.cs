﻿using System;
using System.Configuration;
using System.Windows.Forms;

namespace GraphicalRulesManageConsole
{
    public partial class SettingsForm : Form
    {
        static Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        static ConfigurationSectionGroup applicationSectionGroup = config.GetSectionGroup("applicationSettings");
        static ConfigurationSection applicationConfigSection = applicationSectionGroup.Sections["GraphicalRulesManageConsole.Properties.Settings"];
        static ClientSettingsSection clientSection = (ClientSettingsSection)applicationConfigSection;
        public SettingsForm()
        {
            InitializeComponent();
            camundaModelerTemplatesPathTextBox.Text = clientSection.Settings.Get("camundaTemplatesPath").Value.ValueXml.InnerText;
            openhabItemsPathTextBox.Text = clientSection.Settings.Get("openHabItemsPath").Value.ValueXml.InnerText;
            openhabScriptsPathTextBox.Text = clientSection.Settings.Get("openHabScriptsPath").Value.ValueXml.InnerText;
            bpmnToRulesConverterFilePathTextBox.Text = clientSection.Settings.Get("bpmnToRulesConverterFilePath").Value.ValueXml.InnerText;
            openhabRulesPathTextBox.Text = clientSection.Settings.Get("openHabRulesPath").Value.ValueXml.InnerText;
            camundaModelerExecutableFilePathTextBox.Text = clientSection.Settings.Get("camundaModelerFilePath").Value.ValueXml.InnerText;
            graphicalRulesTextBox.Text = clientSection.Settings.Get("graphicalRulesPath").Value.ValueXml.InnerText;
            openHabUrlTextBox.Text = clientSection.Settings.Get("openhabUrl").Value.ValueXml.InnerText;
        }

        private void changeCamundaModelerTemplatesPathButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    clientSection.Settings.Get("camundaTemplatesPath").Value.ValueXml.InnerText = fbd.SelectedPath;
                    applicationConfigSection.SectionInformation.ForceSave = true;
                    config.Save(ConfigurationSaveMode.Modified);
                    camundaModelerTemplatesPathTextBox.Text = fbd.SelectedPath;
                }
            }
        }

        private void changeOpenhabScriptsPathButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    clientSection.Settings.Get("openHabScriptsPath").Value.ValueXml.InnerText = fbd.SelectedPath;
                    applicationConfigSection.SectionInformation.ForceSave = true;
                    config.Save();
                    openhabScriptsPathTextBox.Text = fbd.SelectedPath;
                }
            }
        }

        private void changeOpenhabItemsPathButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    clientSection.Settings.Get("openHabItemsPath").Value.ValueXml.InnerText = fbd.SelectedPath;
                    applicationConfigSection.SectionInformation.ForceSave = true;
                    config.Save();
                    openhabItemsPathTextBox.Text = fbd.SelectedPath;
                }
            }
        }

        private void changeOpenhabRulesPathButton_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    clientSection.Settings.Get("openHabRulesPath").Value.ValueXml.InnerText = fbd.SelectedPath;
                    applicationConfigSection.SectionInformation.ForceSave = true;
                    config.Save();
                    openhabRulesPathTextBox.Text = fbd.SelectedPath;
                }
            }
        }

        private void changeBpmnToRulesConverterFilePathButton_Click(object sender, EventArgs e)
        {
            var theDialog = new OpenFileDialog
            {
                Title = @"Select BPMN diagram to rules converter file.",
                Filter = @"JAR files|*.jar",
                InitialDirectory = @"C:\"
            };

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                clientSection.Settings.Get("bpmnToRulesConverterFilePath").Value.ValueXml.InnerText = theDialog.FileName;
                applicationConfigSection.SectionInformation.ForceSave = true;
                config.Save();
                bpmnToRulesConverterFilePathTextBox.Text = theDialog.FileName;
            }
        }

        private void changeCamundaModelerExecutableFilePathButton_Click(object sender, EventArgs e)
        {
            var theDialog = new OpenFileDialog
            {
                Title = @"Select Camunda Modeler executable file.",
                Filter = @"EXE files|*.exe",
                InitialDirectory = @"C:\"
            };

            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                clientSection.Settings.Get("camundaModelerFilePath").Value.ValueXml.InnerText = theDialog.FileName;
                applicationConfigSection.SectionInformation.ForceSave = true;
                config.Save();
                camundaModelerExecutableFilePathTextBox.Text = theDialog.FileName;
            }
        }

        private void graphicalRulesPathChangeBtn_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                var result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    clientSection.Settings.Get("graphicalRulesPath").Value.ValueXml.InnerText = fbd.SelectedPath;
                    applicationConfigSection.SectionInformation.ForceSave = true;
                    config.Save();
                    graphicalRulesTextBox.Text = fbd.SelectedPath;
                }
            }
        }

        private void openHabUrlTextBox_TextChanged(object sender, EventArgs e)
        {
            clientSection.Settings.Get("openhabUrl").Value.ValueXml.InnerText = openHabUrlTextBox.Text;
            applicationConfigSection.SectionInformation.ForceSave = true;
            config.Save();
        }
    }
}
