﻿namespace GraphicalRulesManageConsole
{
    partial class GraphicalRulesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GraphicalRulesWindow));
            this.selectFileToConvertButton = new System.Windows.Forms.Button();
            this.newGraphicalRuleButton = new System.Windows.Forms.Button();
            this.editGraphicalRuleButton = new System.Windows.Forms.Button();
            this.rulesDataGridView = new System.Windows.Forms.DataGridView();
            this.graphicalRulesNameLabel = new System.Windows.Forms.Label();
            this.backToMainMenuBtn = new System.Windows.Forms.Button();
            this.searchTextBox = new System.Windows.Forms.TextBox();
            this.validateRuleBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rulesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // selectFileToConvertButton
            // 
            this.selectFileToConvertButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.selectFileToConvertButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.selectFileToConvertButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.selectFileToConvertButton.FlatAppearance.BorderSize = 0;
            this.selectFileToConvertButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.selectFileToConvertButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.selectFileToConvertButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.selectFileToConvertButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.selectFileToConvertButton.ForeColor = System.Drawing.Color.White;
            this.selectFileToConvertButton.Location = new System.Drawing.Point(397, 100);
            this.selectFileToConvertButton.Name = "selectFileToConvertButton";
            this.selectFileToConvertButton.Size = new System.Drawing.Size(160, 40);
            this.selectFileToConvertButton.TabIndex = 3;
            this.selectFileToConvertButton.TabStop = false;
            this.selectFileToConvertButton.Text = "Transform Rule";
            this.selectFileToConvertButton.UseVisualStyleBackColor = false;
            this.selectFileToConvertButton.Click += new System.EventHandler(this.selectFileToConvertButton_Click);
            // 
            // newGraphicalRuleButton
            // 
            this.newGraphicalRuleButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.newGraphicalRuleButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.newGraphicalRuleButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.newGraphicalRuleButton.FlatAppearance.BorderSize = 0;
            this.newGraphicalRuleButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.newGraphicalRuleButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.newGraphicalRuleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newGraphicalRuleButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newGraphicalRuleButton.ForeColor = System.Drawing.Color.White;
            this.newGraphicalRuleButton.Location = new System.Drawing.Point(37, 100);
            this.newGraphicalRuleButton.Name = "newGraphicalRuleButton";
            this.newGraphicalRuleButton.Size = new System.Drawing.Size(160, 40);
            this.newGraphicalRuleButton.TabIndex = 4;
            this.newGraphicalRuleButton.TabStop = false;
            this.newGraphicalRuleButton.Text = "Create Graphical Rule ";
            this.newGraphicalRuleButton.UseVisualStyleBackColor = false;
            this.newGraphicalRuleButton.Click += new System.EventHandler(this.newGraphicalRuleButton_Click);
            // 
            // editGraphicalRuleButton
            // 
            this.editGraphicalRuleButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.editGraphicalRuleButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.editGraphicalRuleButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.editGraphicalRuleButton.FlatAppearance.BorderSize = 0;
            this.editGraphicalRuleButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.editGraphicalRuleButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.editGraphicalRuleButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editGraphicalRuleButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editGraphicalRuleButton.ForeColor = System.Drawing.Color.White;
            this.editGraphicalRuleButton.Location = new System.Drawing.Point(217, 100);
            this.editGraphicalRuleButton.Name = "editGraphicalRuleButton";
            this.editGraphicalRuleButton.Size = new System.Drawing.Size(160, 40);
            this.editGraphicalRuleButton.TabIndex = 5;
            this.editGraphicalRuleButton.TabStop = false;
            this.editGraphicalRuleButton.Text = "Edit Graphical Rule";
            this.editGraphicalRuleButton.UseVisualStyleBackColor = false;
            this.editGraphicalRuleButton.Click += new System.EventHandler(this.editGraphicalRuleButton_Click);
            // 
            // rulesDataGridView
            // 
            this.rulesDataGridView.AllowUserToAddRows = false;
            this.rulesDataGridView.AllowUserToDeleteRows = false;
            this.rulesDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.rulesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rulesDataGridView.Location = new System.Drawing.Point(37, 188);
            this.rulesDataGridView.Name = "rulesDataGridView";
            this.rulesDataGridView.ReadOnly = true;
            this.rulesDataGridView.Size = new System.Drawing.Size(975, 430);
            this.rulesDataGridView.TabIndex = 6;
            this.rulesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.rulesDataGridView_CellClick);
            this.rulesDataGridView.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.rulesDataGridView_CellMouseEnter);
            this.rulesDataGridView.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.rulesDataGridView_CellMouseLeave);
            // 
            // graphicalRulesNameLabel
            // 
            this.graphicalRulesNameLabel.AutoSize = true;
            this.graphicalRulesNameLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.graphicalRulesNameLabel.ForeColor = System.Drawing.Color.White;
            this.graphicalRulesNameLabel.Location = new System.Drawing.Point(381, 31);
            this.graphicalRulesNameLabel.Name = "graphicalRulesNameLabel";
            this.graphicalRulesNameLabel.Size = new System.Drawing.Size(288, 40);
            this.graphicalRulesNameLabel.TabIndex = 7;
            this.graphicalRulesNameLabel.Text = "Graphical Rules";
            // 
            // backToMainMenuBtn
            // 
            this.backToMainMenuBtn.BackColor = System.Drawing.Color.White;
            this.backToMainMenuBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backToMainMenuBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.backToMainMenuBtn.FlatAppearance.BorderSize = 0;
            this.backToMainMenuBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gainsboro;
            this.backToMainMenuBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gainsboro;
            this.backToMainMenuBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backToMainMenuBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backToMainMenuBtn.Location = new System.Drawing.Point(37, 37);
            this.backToMainMenuBtn.Name = "backToMainMenuBtn";
            this.backToMainMenuBtn.Size = new System.Drawing.Size(121, 34);
            this.backToMainMenuBtn.TabIndex = 8;
            this.backToMainMenuBtn.TabStop = false;
            this.backToMainMenuBtn.Text = "< Back";
            this.backToMainMenuBtn.UseVisualStyleBackColor = false;
            this.backToMainMenuBtn.Click += new System.EventHandler(this.backToMainMenuBtn_Click);
            // 
            // searchTextBox
            // 
            this.searchTextBox.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchTextBox.Location = new System.Drawing.Point(37, 155);
            this.searchTextBox.Name = "searchTextBox";
            this.searchTextBox.Size = new System.Drawing.Size(350, 21);
            this.searchTextBox.TabIndex = 9;
            this.searchTextBox.TextChanged += new System.EventHandler(this.searchTextBox_TextChanged);
            this.searchTextBox.Enter += new System.EventHandler(this.searchTextBox_Enter);
            this.searchTextBox.Leave += new System.EventHandler(this.searchTextBox_Leave);
            // 
            // validateRuleBtn
            // 
            this.validateRuleBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.validateRuleBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.validateRuleBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.validateRuleBtn.FlatAppearance.BorderSize = 0;
            this.validateRuleBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.validateRuleBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.validateRuleBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.validateRuleBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.validateRuleBtn.ForeColor = System.Drawing.Color.White;
            this.validateRuleBtn.Location = new System.Drawing.Point(577, 100);
            this.validateRuleBtn.Name = "validateRuleBtn";
            this.validateRuleBtn.Size = new System.Drawing.Size(160, 40);
            this.validateRuleBtn.TabIndex = 10;
            this.validateRuleBtn.Text = "Validate Rule";
            this.validateRuleBtn.UseVisualStyleBackColor = false;
            this.validateRuleBtn.Click += new System.EventHandler(this.validateRuleBtn_Click);
            // 
            // GraphicalRulesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(141)))), ((int)(((byte)(188)))));
            this.ClientSize = new System.Drawing.Size(1034, 641);
            this.ControlBox = false;
            this.Controls.Add(this.validateRuleBtn);
            this.Controls.Add(this.searchTextBox);
            this.Controls.Add(this.backToMainMenuBtn);
            this.Controls.Add(this.graphicalRulesNameLabel);
            this.Controls.Add(this.rulesDataGridView);
            this.Controls.Add(this.editGraphicalRuleButton);
            this.Controls.Add(this.newGraphicalRuleButton);
            this.Controls.Add(this.selectFileToConvertButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GraphicalRulesWindow";
            this.Text = "Graphical Rules";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rulesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button selectFileToConvertButton;
        private System.Windows.Forms.Button newGraphicalRuleButton;
        private System.Windows.Forms.Button editGraphicalRuleButton;
        private System.Windows.Forms.DataGridView rulesDataGridView;
        private System.Windows.Forms.Label graphicalRulesNameLabel;
        private System.Windows.Forms.Button backToMainMenuBtn;
        private System.Windows.Forms.TextBox searchTextBox;
        private System.Windows.Forms.Button validateRuleBtn;
    }
}

