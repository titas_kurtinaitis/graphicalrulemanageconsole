﻿using System.Configuration;
using Newtonsoft.Json;

namespace GraphicalRulesManageConsole.Components
{
    public class JsonObjectManager
    {
        public string UpdateItemsLists(string content, dynamic deserilizedListOfItems)
        {
            return UpdateChoicesByBindingName(content, deserilizedListOfItems, "itemname");
        }

        public string UpdateScriptsList(string content, dynamic deserilizedListOfScripts)
        {
            return UpdateChoicesByBindingName(content, deserilizedListOfScripts, "scriptname");
        }

        public string UpdateGroupsLists(string content, dynamic deserilizedListOfGroups)
        {
            return UpdateChoicesByBindingName(content, deserilizedListOfGroups, "groupname");
        }

        public ClientSettingsSection GetRefreshedApplicationSettingsConfig()
        {
            ConfigurationManager.RefreshSection("applicationSettings");
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var applicationSectionGroup = config.GetSectionGroup("applicationSettings");
            var applicationConfigSection = applicationSectionGroup.Sections["GraphicalRulesManageConsole.Properties.Settings"];
            return (ClientSettingsSection)applicationConfigSection;
        }

        private string UpdateChoicesByBindingName(string content, dynamic deserilizedListOfScripts, string bindingName)
        {
            dynamic jsonObjs = JsonConvert.DeserializeObject(content);
            foreach (var jsonObj in jsonObjs)
            {
                var properties = jsonObj["properties"];
                foreach (var property in properties)
                {
                    if (property["type"].ToString().Equals("Dropdown") && property["binding"]["name"].ToString().Equals(bindingName))
                    {
                        property["choices"] = deserilizedListOfScripts;
                    }
                }
            }
            var output = JsonConvert.SerializeObject(jsonObjs, Formatting.Indented);

            return output;
        }
    }
}
