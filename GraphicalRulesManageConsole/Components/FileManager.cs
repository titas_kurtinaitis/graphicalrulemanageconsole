﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GraphicalRulesManageConsole.Components
{
    public class FileManager
    {
        public IEnumerable<string> GetFilesByExtension(string extension, string path)
        {
            var files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories)
                .Where(f => extension.Equals(Path.GetExtension(f)));

            return files;
        }

        public string GetFileByName(string fileName, string path)
        {
            var file = Directory
                .GetFiles(path, "*.*", SearchOption.AllDirectories).FirstOrDefault(f => fileName.Equals(Path.GetFileNameWithoutExtension(f)));

            return file;
        }

        public string GetFileContent(string path)
        {
            return File.ReadAllText(path);
        }

        public void UpdateFileContent(string path, string newContent)
        {
            File.WriteAllText(path, newContent);
        }
    }
}
