﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace GraphicalRulesManageConsole.Components
{
    public class CmdCommandsManager
    {
        JsonObjectManager jsonObjectManager = new JsonObjectManager();
        public void RunBpmnFileConverter(string fileToConvert, bool onlyValidation)
        {
            //needed for file names with white spaces
           // fileToConvert = "\"" + fileToConvert + "\"";
            var process = new Process();
            var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
            var bpmnRulesConverterPath = clientSection.Settings.Get("bpmnToRulesConverterFilePath").Value.ValueXml.InnerText;
            var openHabRulesPath = clientSection.Settings.Get("openHabRulesPath").Value.ValueXml.InnerText;
            var startInfo =
                new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments = $@"/C java -jar ""{bpmnRulesConverterPath}"" ""{fileToConvert}"" ""{openHabRulesPath}"" {onlyValidation}",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
            };
            process.StartInfo = startInfo;
            process.Start();
            //* Read the output (or the error)
            var err = process.StandardError.ReadToEnd();
            process.WaitForExit();

            if (string.IsNullOrEmpty(err))
            {
                var successMessage = onlyValidation
                    ? @"Graphical rule does not have validation errors."
                    : @"Graphical rule was transformed successfully.";
                MessageBox.Show(successMessage, @"Success", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
            }
            else
            {
                var substringFrom = err.IndexOf("Exception: ", StringComparison.InvariantCulture) + "Exception: ".Length;
                var substringTo = err.IndexOf("at com.company", StringComparison.InvariantCulture);
                var errorString = err.Substring(substringFrom, substringTo - substringFrom);
                var errorMessageToShow = onlyValidation ? "Graphical rule has validation error: \n\n" + errorString : "Cannot transform graphical rule because of: \n\n " + errorString;
                MessageBox.Show(errorMessageToShow, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void OpenCamundaModeler(string existingBpmnDiagramFile = "")
        {
            //needed for file names with white spaces
            existingBpmnDiagramFile = "\"" + existingBpmnDiagramFile + "\"";
            var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
            var camundaModelerFilePath = "\"" + clientSection.Settings.Get("camundaModelerFilePath").Value.ValueXml.InnerText + "\"";
            var startInfo =
                new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = camundaModelerFilePath,
                    Arguments = existingBpmnDiagramFile
                };

            Process.Start(startInfo);
        }
    }
}
