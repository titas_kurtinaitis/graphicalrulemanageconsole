﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GraphicalRulesManageConsole.Enums;
using GraphicalRulesManageConsole.Models;

namespace GraphicalRulesManageConsole.Components
{
    public class ItemsManager
    {
        private FileManager fileManager = new FileManager();
        JsonObjectManager jsonObjectManager = new JsonObjectManager();
        public List<DropdownPair> GetListOfItems(bool findOnlyGroups)
        {
            var listOfItems = new List<DropdownPair>();
            var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
            var openHabItemsPath = clientSection.Settings.Get("openHabItemsPath").Value.ValueXml.InnerText;
            var itemFiles = fileManager.GetFilesByExtension(".items", openHabItemsPath);
            foreach (var itemFile in itemFiles)
            {
                ReadLineByLine(itemFile, listOfItems, findOnlyGroups);
            }
            return listOfItems;
        }

        private void ReadLineByLine(string file, List<DropdownPair> itemsList, bool findOnlyGroups)
        {
            string line;
            var fileContent = new StreamReader(file);
            while ((line = fileContent.ReadLine()) != null)
            {
                if (!line.Equals(""))
                {
                    AddItemToList(line, itemsList, findOnlyGroups);
                }
            }
        }

        private void AddItemToList(string line, List<DropdownPair> itemsList, bool findOnlyGroups)
        {
            var words = line.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            var itemDeclaration = words.Take(2);
            var declaration = itemDeclaration as string[] ?? itemDeclaration.ToArray();
            var itemType = declaration.First();
            var itemName = declaration.Last();
            if (Enum.GetNames(typeof(ItemTypes)).Contains(itemType) && !itemType.Contains(ItemTypes.Group.ToString()))
            {
                //adds items only without groups
                if (!findOnlyGroups)
                {
                    var item = new DropdownPair { name = itemName + $" (type of {itemType.ToUpper()})", value = itemName + "|" + itemType.ToUpper() };
                    itemsList.Add(item);
                }   
            }
            else if (itemType.Equals(ItemTypes.Group.ToString()))
            {
                //adds groups
                if (findOnlyGroups)
                {
                    var item = new DropdownPair { name = itemName, value = itemName };
                    itemsList.Add(item);
                }
            }
        }
    }
}
