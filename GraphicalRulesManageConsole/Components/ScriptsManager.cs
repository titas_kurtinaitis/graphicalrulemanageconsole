﻿using System.Collections.Generic;
using System.IO;
using GraphicalRulesManageConsole.Models;

namespace GraphicalRulesManageConsole.Components
{
    public class ScriptsManager
    {
        private FileManager fileManager = new FileManager();
        JsonObjectManager jsonObjectManager = new JsonObjectManager();
        public List<DropdownPair> GetListOfScripts()
        {
            var listOfScripts = new List<DropdownPair>();
            var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
            var openHabScriptsPath = clientSection.Settings.Get("openHabScriptsPath").Value.ValueXml.InnerText;
            var scriptFiles = fileManager.GetFilesByExtension(".script", openHabScriptsPath);
            foreach (var scriptFile in scriptFiles)
            {
                AddScriptFile(scriptFile, listOfScripts);
            }

            return listOfScripts;
        }

        private void AddScriptFile(string scriptFile, List<DropdownPair> listOfScripts)
        {
            var scriptFileName = Path.GetFileNameWithoutExtension(scriptFile);
            var dropdownPair = new DropdownPair() {name = scriptFileName, value = scriptFileName};
            listOfScripts.Add(dropdownPair);
        }
    }
}
