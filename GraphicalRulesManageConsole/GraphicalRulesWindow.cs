﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using GraphicalRulesManageConsole.Components;
using GraphicalRulesManageConsole.Models;
using Newtonsoft.Json;

namespace GraphicalRulesManageConsole
{
    public partial class GraphicalRulesWindow : Form
    {
        private FileManager fileManager = new FileManager();
        private BindingSource rulesBindingSource = new BindingSource();
        private Action showMainForm;
        private const string searchPlaceHolderText = @"Search by rule name...";
        JsonObjectManager jsonObjectManager = new JsonObjectManager();

        public GraphicalRulesWindow(Action showMainForm)
        {
            InitializeComponent();
            this.showMainForm = showMainForm;
            searchTextBox.Text = searchPlaceHolderText;
        }

        private void selectFileToConvertButton_Click(object sender, EventArgs e)
        {
            var cmdCommandsManager = new CmdCommandsManager();
            var theDialog = new OpenFileDialog
            {
                Title = @"Open BPMN file to convert",
                Filter = @"BPMN files|*.bpmn",
                InitialDirectory = @"C:\"
            };
            //TODO: change to initial directory where BPMN FILES WILL BE SERVED
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                cmdCommandsManager.RunBpmnFileConverter(theDialog.FileName, false);
            }
        }

        private void newGraphicalRuleButton_Click(object sender, EventArgs e)
        {
            var newRuleForm = new NewRuleForm();
            newRuleForm.StartPosition = FormStartPosition.CenterScreen;
            newRuleForm.Show();
        }

        private void editGraphicalRuleButton_Click(object sender, EventArgs e)
        {
            var cmdCommandsManager = new CmdCommandsManager();
            var theDialog = new OpenFileDialog
            {
                Title = @"Select graphical rule's BPMN file to edit",
                Filter = @"BPMN files|*.bpmn",
                InitialDirectory = @"C:\"
            };
            //TODO: change to initial directory where BPMN FILES WILL BE SERVED
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                cmdCommandsManager.OpenCamundaModeler(theDialog.FileName);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UpdateDataSource();
            rulesDataGridView.AutoGenerateColumns = false;
            rulesDataGridView.AutoSize = false;
            rulesDataGridView.RowHeadersVisible = false;
            rulesDataGridView.AllowUserToResizeRows = false;
            rulesDataGridView.RowTemplate.Height = 30;
            rulesDataGridView.DataSource = rulesBindingSource;

            DataGridViewColumn ruleNameColumn = new DataGridViewTextBoxColumn();
            ruleNameColumn.DataPropertyName = "ruleName";
            ruleNameColumn.Name = "Rule Name";
            ruleNameColumn.Width = 200;
            ruleNameColumn.CellTemplate.Style.Font = new Font("Arial Rounded MT", 10);
            rulesDataGridView.Columns.Add(ruleNameColumn);


            DataGridViewColumn rulePathColumn = new DataGridViewTextBoxColumn();
            rulePathColumn.DataPropertyName = "rulePath";
            rulePathColumn.Name = "Path of the Graphical Rule";
            rulePathColumn.Width = 371;
            rulePathColumn.CellTemplate.Style.Font = new Font("Arial Rounded MT", 10);
            rulesDataGridView.Columns.Add(rulePathColumn);

            DataGridViewButtonColumn btnEditGraphicalRule = new DataGridViewButtonColumn();
            btnEditGraphicalRule.HeaderText = "";
            btnEditGraphicalRule.Name = "colEdit";
            btnEditGraphicalRule.Text = "View / Edit";
            btnEditGraphicalRule.UseColumnTextForButtonValue = true;
            btnEditGraphicalRule.Width = 100;
            btnEditGraphicalRule.FlatStyle = FlatStyle.Flat;
            btnEditGraphicalRule.CellTemplate.Style.BackColor = Color.FromArgb(51, 51, 51);
            btnEditGraphicalRule.CellTemplate.Style.ForeColor = Color.White;
            btnEditGraphicalRule.CellTemplate.Style.Font = new Font("Arial Rounded MT", 10);
            rulesDataGridView.Columns.Add(btnEditGraphicalRule);

            DataGridViewButtonColumn btnTransformGraphicalRule = new DataGridViewButtonColumn();
            btnTransformGraphicalRule.HeaderText = "";
            btnTransformGraphicalRule.Name = "colTransform";
            btnTransformGraphicalRule.Text = "Transform";
            btnTransformGraphicalRule.UseColumnTextForButtonValue = true;
            btnTransformGraphicalRule.Width = 100;
            btnTransformGraphicalRule.FlatStyle = FlatStyle.Flat;
            btnTransformGraphicalRule.CellTemplate.Style.BackColor = Color.Green;
            btnTransformGraphicalRule.CellTemplate.Style.ForeColor = Color.White;
            btnTransformGraphicalRule.CellTemplate.Style.Font = new Font("Arial Rounded MT", 10);
            rulesDataGridView.Columns.Add(btnTransformGraphicalRule);

            DataGridViewButtonColumn btnValidateGraphicalRule = new DataGridViewButtonColumn();
            btnValidateGraphicalRule.HeaderText = "";
            btnValidateGraphicalRule.Name = "colValidate";
            btnValidateGraphicalRule.Text = "Validate";
            btnValidateGraphicalRule.UseColumnTextForButtonValue = true;
            btnValidateGraphicalRule.Width = 100;
            btnValidateGraphicalRule.FlatStyle = FlatStyle.Flat;
            btnValidateGraphicalRule.CellTemplate.Style.BackColor = Color.DarkOrange;
            btnValidateGraphicalRule.CellTemplate.Style.ForeColor = Color.White;
            btnValidateGraphicalRule.CellTemplate.Style.Font = new Font("Arial Rounded MT", 10);
            rulesDataGridView.Columns.Add(btnValidateGraphicalRule);

            DataGridViewButtonColumn btnDeleteGraphicalRule = new DataGridViewButtonColumn();
            btnDeleteGraphicalRule.HeaderText = "";
            btnDeleteGraphicalRule.Name = "colDelete";
            btnDeleteGraphicalRule.Text = "Delete";
            btnDeleteGraphicalRule.UseColumnTextForButtonValue = true;
            btnDeleteGraphicalRule.Width = 100;
            btnDeleteGraphicalRule.FlatStyle = FlatStyle.Flat;
            btnDeleteGraphicalRule.CellTemplate.Style.BackColor = Color.Red;
            btnDeleteGraphicalRule.CellTemplate.Style.ForeColor = Color.White;
            btnDeleteGraphicalRule.CellTemplate.Style.Font = new Font("Arial Rounded MT", 10);
            rulesDataGridView.Columns.Add(btnDeleteGraphicalRule);

            //define file system watchers
            WatchGraphicalRulesFolder();
        }

        private void rulesDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //view / edit of graphical rule
            var editColumn = rulesDataGridView.Columns["colEdit"];
            if (editColumn != null && e.ColumnIndex == editColumn.Index)
            {
                var cmdCommandsManager = new CmdCommandsManager();
                if (e.RowIndex > -1)
                {
                    var graphicalRule = (ActivityRule)rulesBindingSource.List[e.RowIndex];
                    cmdCommandsManager.OpenCamundaModeler(graphicalRule.RulePath);
                }           
            }

            //transform
            var transformColumn = rulesDataGridView.Columns["colTransform"];
            if (transformColumn != null && e.ColumnIndex == transformColumn.Index)
            {
                if (e.RowIndex > -1)
                {
                    var graphicalRule = (ActivityRule)rulesBindingSource.List[e.RowIndex];
                    if (DialogResult.Yes == MessageBox.Show($@"Do you want to transform the ""{graphicalRule.RuleName}"" graphical rule?", "", MessageBoxButtons.YesNo))
                    {
                        var cmdCommandsManager = new CmdCommandsManager();
                        cmdCommandsManager.RunBpmnFileConverter(graphicalRule.RulePath, false);
                    }
                }                   
            }

            //validate graphical rule
            var validateColumn = rulesDataGridView.Columns["colValidate"];
            if (validateColumn != null && e.ColumnIndex == validateColumn.Index)
            {
                var graphicalRule = (ActivityRule)rulesBindingSource.List[e.RowIndex];
                var cmdCommandsManager = new CmdCommandsManager();
                cmdCommandsManager.RunBpmnFileConverter(graphicalRule.RulePath, true);
            }

            //delete graphical rule
            var deleteColumn = rulesDataGridView.Columns["colDelete"];
            if (deleteColumn != null && e.ColumnIndex == deleteColumn.Index)
            {
                var graphicalRule = (ActivityRule) rulesBindingSource.List[e.RowIndex];
                if (DialogResult.Yes == MessageBox.Show(
                        $@"Do tou want to delete ""{graphicalRule.RuleName}"" graphical rule?", "",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                {
                    File.Delete(graphicalRule.RulePath);
                }
            }
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            UpdateDataSource();
        }


        BindingList<ActivityRule> bindingList = new BindingList<ActivityRule>();
        private void UpdateDataSource()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(UpdateDataSource));
            }
            else
            {
                var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
                var graphicalRulesPath = clientSection.Settings.Get("graphicalRulesPath").Value.ValueXml.InnerText;
                var ruleFiles = fileManager.GetFilesByExtension(".bpmn", graphicalRulesPath);
                var list = ruleFiles.Select(ruleFile => new ActivityRule { RuleName = Path.GetFileNameWithoutExtension(ruleFile), RulePath = ruleFile }).ToList();
                bindingList = new BindingList<ActivityRule>(list);
                rulesBindingSource.DataSource = bindingList;
                rulesBindingSource.DataMember = null;
            }
            
        }

        private void WatchGraphicalRulesFolder()
        {
            var watcher = new FileSystemWatcher();
            var clientSection = jsonObjectManager.GetRefreshedApplicationSettingsConfig();
            var graphicalRulesPath = clientSection.Settings.Get("graphicalRulesPath").Value.ValueXml.InnerText;
            watcher.Path = graphicalRulesPath;
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
                                   | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Filter = "*.bpmn";
            watcher.Changed += OnChanged;
            watcher.Created += OnChanged;
            watcher.Deleted += OnChanged;
            watcher.Renamed += OnChanged;
            watcher.EnableRaisingEvents = true;
        }

        private void backToMainMenuBtn_Click(object sender, EventArgs e)
        {
            Close();
            showMainForm();          
        }

        private void searchTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!searchTextBox.Text.Equals(searchPlaceHolderText))
            {
                var filter = searchTextBox.Text.Trim().Replace("'", "''");
                rulesDataGridView.DataSource = new BindingList<ActivityRule>(bindingList.Where(m => m.RuleName.IndexOf(filter, StringComparison.OrdinalIgnoreCase) >= 0).ToList());
            }
            
        }

        private void searchTextBox_Enter(object sender, EventArgs e)
        {
            if (searchTextBox.Text.Equals(searchPlaceHolderText))
            {
                searchTextBox.Text = "";
            }
        }

        private void searchTextBox_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(searchTextBox.Text))
            {
                searchTextBox.Text = searchPlaceHolderText;
            }
        }

        private void validateRuleBtn_Click(object sender, EventArgs e)
        {
            var cmdCommandsManager = new CmdCommandsManager();
            var theDialog = new OpenFileDialog
            {
                Title = @"Open BPMN file to validate",
                Filter = @"BPMN files|*.bpmn",
                InitialDirectory = @"C:\"
            };
            //TODO: change to initial directory where BPMN FILES WILL BE SERVED
            if (theDialog.ShowDialog() == DialogResult.OK)
            {
                cmdCommandsManager.RunBpmnFileConverter(theDialog.FileName, true);
            }
        }

        private void rulesDataGridView_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex < 0 || e.RowIndex < 0)
            {
                return;
            }
            var dataGridView = sender as DataGridView;
            //Check the condition as per the requirement casting the cell value to the appropriate type
            if (dataGridView != null && e.ColumnIndex >= 2)
            {
                dataGridView.Cursor = Cursors.Hand;
            }
            else if (dataGridView != null)
            {
                dataGridView.Cursor = Cursors.Default;
            }
        }

        private void rulesDataGridView_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            var dataGridView = sender as DataGridView;
            if (dataGridView != null) dataGridView.Cursor = Cursors.Default;
        }
    }
}
