﻿namespace GraphicalRulesManageConsole
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.appNameLabel = new System.Windows.Forms.Label();
            this.showGraphicalRulesBtn = new System.Windows.Forms.Button();
            this.transformedRulesBtn = new System.Windows.Forms.Button();
            this.updateItemsAndGroupsBtn = new System.Windows.Forms.Button();
            this.updateScriptsBtn = new System.Windows.Forms.Button();
            this.settingsBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // appNameLabel
            // 
            this.appNameLabel.AutoSize = true;
            this.appNameLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appNameLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.appNameLabel.Location = new System.Drawing.Point(176, 34);
            this.appNameLabel.Name = "appNameLabel";
            this.appNameLabel.Size = new System.Drawing.Size(816, 40);
            this.appNameLabel.TabIndex = 0;
            this.appNameLabel.Text = "openHAB graphical rules management console";
            // 
            // showGraphicalRulesBtn
            // 
            this.showGraphicalRulesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.showGraphicalRulesBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.showGraphicalRulesBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.showGraphicalRulesBtn.FlatAppearance.BorderSize = 0;
            this.showGraphicalRulesBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.showGraphicalRulesBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.showGraphicalRulesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.showGraphicalRulesBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showGraphicalRulesBtn.ForeColor = System.Drawing.Color.White;
            this.showGraphicalRulesBtn.Location = new System.Drawing.Point(275, 100);
            this.showGraphicalRulesBtn.Name = "showGraphicalRulesBtn";
            this.showGraphicalRulesBtn.Size = new System.Drawing.Size(500, 60);
            this.showGraphicalRulesBtn.TabIndex = 1;
            this.showGraphicalRulesBtn.TabStop = false;
            this.showGraphicalRulesBtn.Text = "Show Graphical Rules";
            this.showGraphicalRulesBtn.UseVisualStyleBackColor = false;
            this.showGraphicalRulesBtn.Click += new System.EventHandler(this.showGraphicalRulesBtn_Click);
            // 
            // transformedRulesBtn
            // 
            this.transformedRulesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.transformedRulesBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.transformedRulesBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.transformedRulesBtn.FlatAppearance.BorderSize = 0;
            this.transformedRulesBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.transformedRulesBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.transformedRulesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.transformedRulesBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transformedRulesBtn.ForeColor = System.Drawing.Color.White;
            this.transformedRulesBtn.Location = new System.Drawing.Point(275, 190);
            this.transformedRulesBtn.Name = "transformedRulesBtn";
            this.transformedRulesBtn.Size = new System.Drawing.Size(500, 60);
            this.transformedRulesBtn.TabIndex = 3;
            this.transformedRulesBtn.TabStop = false;
            this.transformedRulesBtn.Text = "Show Transformed Rules";
            this.transformedRulesBtn.UseVisualStyleBackColor = false;
            this.transformedRulesBtn.Click += new System.EventHandler(this.transformedRulesBtn_Click);
            // 
            // updateItemsAndGroupsBtn
            // 
            this.updateItemsAndGroupsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.updateItemsAndGroupsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updateItemsAndGroupsBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.updateItemsAndGroupsBtn.FlatAppearance.BorderSize = 0;
            this.updateItemsAndGroupsBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.updateItemsAndGroupsBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.updateItemsAndGroupsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateItemsAndGroupsBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateItemsAndGroupsBtn.ForeColor = System.Drawing.Color.White;
            this.updateItemsAndGroupsBtn.Location = new System.Drawing.Point(275, 280);
            this.updateItemsAndGroupsBtn.Name = "updateItemsAndGroupsBtn";
            this.updateItemsAndGroupsBtn.Size = new System.Drawing.Size(500, 60);
            this.updateItemsAndGroupsBtn.TabIndex = 4;
            this.updateItemsAndGroupsBtn.TabStop = false;
            this.updateItemsAndGroupsBtn.Text = "Update Items and Groups";
            this.updateItemsAndGroupsBtn.UseVisualStyleBackColor = false;
            this.updateItemsAndGroupsBtn.Click += new System.EventHandler(this.updateItemsAndGroupsBtn_Click);
            // 
            // updateScriptsBtn
            // 
            this.updateScriptsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.updateScriptsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.updateScriptsBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.updateScriptsBtn.FlatAppearance.BorderSize = 0;
            this.updateScriptsBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.updateScriptsBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.updateScriptsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateScriptsBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateScriptsBtn.ForeColor = System.Drawing.Color.White;
            this.updateScriptsBtn.Location = new System.Drawing.Point(275, 370);
            this.updateScriptsBtn.Name = "updateScriptsBtn";
            this.updateScriptsBtn.Size = new System.Drawing.Size(500, 60);
            this.updateScriptsBtn.TabIndex = 5;
            this.updateScriptsBtn.TabStop = false;
            this.updateScriptsBtn.Text = "Update Scripts";
            this.updateScriptsBtn.UseVisualStyleBackColor = false;
            this.updateScriptsBtn.Click += new System.EventHandler(this.updateScriptsBtn_Click);
            // 
            // settingsBtn
            // 
            this.settingsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.settingsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.settingsBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.settingsBtn.FlatAppearance.BorderSize = 0;
            this.settingsBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.settingsBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.settingsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.settingsBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingsBtn.ForeColor = System.Drawing.Color.White;
            this.settingsBtn.Location = new System.Drawing.Point(275, 460);
            this.settingsBtn.Name = "settingsBtn";
            this.settingsBtn.Size = new System.Drawing.Size(500, 60);
            this.settingsBtn.TabIndex = 6;
            this.settingsBtn.TabStop = false;
            this.settingsBtn.Text = "Settings";
            this.settingsBtn.UseVisualStyleBackColor = false;
            this.settingsBtn.Click += new System.EventHandler(this.settingsBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.exitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.exitBtn.FlatAppearance.BorderSize = 0;
            this.exitBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.exitBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitBtn.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.ForeColor = System.Drawing.Color.White;
            this.exitBtn.Location = new System.Drawing.Point(275, 550);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(500, 60);
            this.exitBtn.TabIndex = 7;
            this.exitBtn.TabStop = false;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GraphicalRulesManageConsole.Properties.Resources._1007353;
            this.pictureBox1.Location = new System.Drawing.Point(63, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(107, 112);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(141)))), ((int)(((byte)(188)))));
            this.ClientSize = new System.Drawing.Size(1034, 641);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.settingsBtn);
            this.Controls.Add(this.updateScriptsBtn);
            this.Controls.Add(this.updateItemsAndGroupsBtn);
            this.Controls.Add(this.transformedRulesBtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.showGraphicalRulesBtn);
            this.Controls.Add(this.appNameLabel);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "openHAB graphical rules management console";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label appNameLabel;
        private System.Windows.Forms.Button showGraphicalRulesBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button transformedRulesBtn;
        private System.Windows.Forms.Button updateItemsAndGroupsBtn;
        private System.Windows.Forms.Button updateScriptsBtn;
        private System.Windows.Forms.Button settingsBtn;
        private System.Windows.Forms.Button exitBtn;
    }
}